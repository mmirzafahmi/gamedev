//============================================================================
// Name        : Main.cpp
// Author      : M Mirza Fahmi
// Version     :
// Copyright   : Your copyright notice
// Description : Space Invader
//============================================================================

#include <iostream>
#include "ScreenUtil.h"
#include "GameData.h"
//#include "GameSystem.h"

using namespace std;

const char * PLAYER_SPRITE[] = {"=A=","====="};

void InitGame(Game &game);
void InitPlayer(const Game &game, Player &player);
void ResetPlayer(const Game &game, Player &player);
void ResetMissile(Player &player);
char HandleInput(const Game &game, Player &player);
void UpdateGame(const Game &game, Player &player);
void DrawGame(const Game &game, const Player &player);

int main() {

	//GameSystem gs;
	Game game;
	Player player;

	InitializeScreen(true);

	InitGame(game);
	InitPlayer(game, player);

	bool quit = false;
	//char input;

	while(!quit){

		//input = HandleInput(game, player);
		UpdateGame(game, player);
		ClearScreen();
		DrawGame(game, player);
		RefreshScreen();

	}


	return 0;
}

void InitGame(Game &game){
	game.windowSize.width = ScreenWidth();
	game.windowSize.height = ScreenHeight();
	game.level = 1;
	game.currentState = GS_PLAY;
}

void InitPlayer(const Game &game, Player &player){
	player.lives = MAX_NUM_OF_LIVES;
	player.spriteSize.width = PLAYER_SPRITE_WIDTH;
	player.spriteSize.height = PLAYER_SPRITE_HEIGHT;
	ResetPlayer(game, player);
}

void ResetPlayer(const Game &game, Player &player){
	player.coordinate.xPos = game.windowSize.width / 2 - player.spriteSize.width / 2;
	player.coordinate.yPos = game.windowSize.height - player.spriteSize.height - 1;
	player.animation = 0;
	ResetMissile(player);
}

void ResetMissile(Player &player){
	player.missile.xPos = NOT_IN_PLAY;
	player.missile.yPos = NOT_IN_PLAY;
}

char HandleInput(const Game &game, Player &player){
	return ' ';
}

void UpdateGame(const Game &game, Player &player){

}

void DrawGame(const Game &game, const Player &player){
	DrawSprite(player.coordinate.xPos, player.coordinate.yPos, PLAYER_SPRITE, player.spriteSize.height);
}

