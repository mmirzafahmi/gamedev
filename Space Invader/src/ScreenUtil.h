/*
 * ScreenUtil.h
 *
 *  Created on: Dec 11, 2016
 *      Author: root
 */

#ifndef SCREENUTIL_H_
#define SCREENUTIL_H_
#include <ncurses.h>

enum ArrowKeys{
	UP = KEY_UP,
	DOWN = KEY_DOWN,
	LEFT = KEY_LEFT,
	RIGHT = KEY_RIGHT
};

void InitializeScreen(bool noDelay);
void Shutdown();
void ClearScreen();
void RefreshScreen();
int ScreenWidth();
int ScreenHeight();
int getChar();
void WriteCharacter(int xPos, int yPos, char character);
void MoveCursor();
void DrawSprite(int xPos, int yPos, const char * sprite[], int spriteHeight, int offset = 0);

#endif /* SCREENUTIL_H_ */
