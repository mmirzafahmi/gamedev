/*
 * ScreenUtil.cpp
 *
 *  Created on: Dec 11, 2016
 *      Author: root
 */

#include "ScreenUtil.h"

void InitializeScreen(bool noDelay){
	initscr();
	noecho();
	curs_set(false);
	nodelay(stdscr, noDelay);
	keypad(stdscr, true);
}

void Shutdown(){
	endwin();
}

void ClearScreen(){
	clear();
}

void RefreshScreen(){
	refresh();
}

int ScreenWidth(){
	return COLS;
}

int ScreenHeight(){
	return LINES;
}

int getChar(){
	return getch();
}

void WriteCharacter(int xPos, int yPos, char character){
	mvaddch(xPos, yPos, character);
}

void MoveCursor(int xPos, int yPos){
	move(yPos, xPos);
}

void DrawSprite(int xPos, int yPos, const char * sprite[], int spriteHeight, int offset){

	for(int h = 0; h < spriteHeight; h++){
		mvprintw(yPos + h, xPos, "%s", sprite[h]);
	}
}
