/*
 * GameSystem.h
 *
 *  Created on: Dec 11, 2016
 *      Author: root
 */

#ifndef GAMESYSTEM_H_
#define GAMESYSTEM_H_
#include "GameData.h"
#include "ScreenUtil.h"

using namespace std;

class GameSystem {
public:
	void InitGame(Game &game);
	void InitPlayer(const Game &game, Player &player);
	void ResetPlayer(const Game &game, Player &player);
	void ResetMissile(Player &player);
	char HandleInput(const Game &game, Player &player);
	void UpdateGame(const Game &game, Player &player);
	void DrawGame(const Game &game, const Player &player);

};

#endif /* GAMESYSTEM_H_ */
