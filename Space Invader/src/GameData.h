/*
 * GameData.h
 *
 *  Created on: Dec 11, 2016
 *      Author: root
 */

#ifndef GAMEDATA_H_
#define GAMEDATA_H_
#include <iostream>
#include <vector>

using namespace std;



enum{
	SHIELD_SPRITE_HEIGHT = 3,
	NUM_ALIENS_ROW = 5,
	NUM_ALIENS_COLUMN = 11,
	MAX_NUM_ALIEN_BOMBS = 3,
	MAX_NUM_OF_LIVES = 3,
	PLAYER_SPRITE_WIDTH = 5,
	PLAYER_SPRITE_HEIGHT = 2,
	NOT_IN_PLAY
};

enum AlienState{
	AS_ALIVE = 0,
	AS_DEAD,
	AS_EXPLODING
};

enum GameState{
	GS_INTRO = 0,
	GS_HIGHSCORES,
	GS_PLAY,
	GS_PLAYER_DEAD,
	GS_WAIT,
	GS_GAMEOVER
};

struct Position{
	int xPos;
	int yPos;
};

struct Size{
	int width;
	int height;
};

struct Player{
	Position coordinate;
	Position missile;
	Size spriteSize;
	int animation;
	int lives;
	int score;
};

struct Shield{
	Position coordinate;
	char * sprite[SHIELD_SPRITE_HEIGHT];
};

struct AlienBomb{
	Position coordinate;
	int animation;
};

struct AlienSwarm{
	Position coordinate;
	AlienState aliens[NUM_ALIENS_ROW][NUM_ALIENS_COLUMN];
	AlienBomb bomb[MAX_NUM_ALIEN_BOMBS];
	Size spriteSize;
	int animation;
	int direction;
	int numOfBombsInPlay;
	int movementTime;
	int exlodingTime;
	int numAliensLeft;
	int line;
};

struct AlienUFO{
	Position coordinate;
	Size size;
	int points;
};

struct Score{
	int score;
	string name;
};

struct HighScoreTable{
	vector<Score> HighScore;
};

struct Game{
	Size windowSize;
	GameState currentState;
	int level;
};

#endif /* GAMEDATA_H_ */
