/*
 * Maps.h
 *
 *  Created on: Dec 10, 2016
 *      Author: root
 */

#ifndef MAPS_H_
#define MAPS_H_
#include <iostream>
#include <vector>
#include <cstring>
#include "Wall.h"
#include "Food.h"

using namespace std;

class Maps {
public:
	Maps();
	void Load(string fileName);
	//setters
	void setTile(char tile, int tileX, int tileY);

	//getters
	char getTile(int tileX, int tileY);


private:
	void createMap();
	void Dig();

	vector<string> mapData;
	vector<Wall> wall;
	vector<Food> food;

};

#endif /* MAPS_H_ */
